// Custom JS

// Match height

$(function() {
	$('.match-height').matchHeight(
	{
		byRow: true,
		property: 'height',
		target: null,
		remove: false
	}
	);
});